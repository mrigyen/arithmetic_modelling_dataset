package main

import "os"
import "strconv"
import "math/rand"
import "time"

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func mapNumArange(fun func(int, int) int, num int, upperLimit int) [][]int {
	ret := make([][]int, upperLimit)
	for i := 0; i < upperLimit; i++ {
		ret[i] = make([]int, 3)
		ret[i][0] = num
		ret[i][1] = i
		ret[i][2] = fun(num, i)
	}
	return ret
}

func generatePartition(fun func(int, int) int, num int, upperLimit int, op string, operation_name string) {
	partition := mapNumArange(fun, num, upperLimit)
	fileNamePrefix := operation_name + "_" + strconv.Itoa(num)
	fTrain, err := os.Create("train_" + fileNamePrefix + ".jsonl")
	check(err)
	defer fTrain.Close()
	fTest, err := os.Create("test_" + fileNamePrefix + ".jsonl")
	check(err)
	defer fTest.Close()
	for i := range partition {
		str := strconv.Itoa(partition[i][0]) + op + strconv.Itoa(partition[i][1]) + "=" + strconv.Itoa(partition[i][2])
		jsonlStr := "{\"text\":\"" + str + "\"}" + "\n"
		if rand.Float64() < 0.70 {
			fTrain.WriteString(jsonlStr)
		} else {
			fTest.WriteString(jsonlStr)
		}
	}
}

func add(a int, b int) int {
	return a + b
}

func subtract(a int, b int) int {
	return a - b
}

func main() {
	upperLimit := 10000
	for i := 0; i < upperLimit; i++ {
		go generatePartition(add, i, upperLimit, "+", "add")
		go generatePartition(subtract, i, upperLimit, "-", "subtract")
	}
	time.Sleep(time.Second)
}
