#!/usr/bin/env bash

mkdir output
cat train_add* > output/train_add.jsonl
cat test_add* > output/test_add.jsonl
cat train_subtract* > output/train_subtract.jsonl
cat test_subtract* > output/test_subtract.jsonl
rm *.jsonl
